package hw4;

import java.util.Arrays;
import java.util.Random;

/**
 * The Pet class must display the
 * following message: dog{nickname='Rock', age=5, trickLevel=75, habits=[eat, drink, sleep]}, where dog = species;
 *
 * */

class Pet {
    private String nickname,species;
    private Integer age, trickLevel;
    private String[] habbits = new String[3];
    public Pet(String[] habbits, String nickname, int age, int trickLevel) {
        this.habbits = habbits;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
    }
    public String getNickname() { return this.nickname; }
    public int getAge()  { return this.age; }
    public int getTrickLevel()  { return this.trickLevel; }
    public String getSpecies(){return this.species;}
    public void eat() { System.out.println("I am eating"); }

    public void respond() { System.out.printf("Hello, owner. I am %s. I miss you!", this.nickname); }

    public void foul() { System.out.println("I need to cover it up"); }


    @Override
    public String toString() {
        System.out.printf("dog{nickname='%s', age = %d, trickLevel = %d, habit=[%s]}, where dog = species;",
                this.nickname,this.age,this.trickLevel, Arrays.toString(this.habbits) );
        return null;
    }
}

class Human{
    private String name, surname;
    private Integer IQ, dateOfBirth;
    private Pet pet;
    private Human mother, father;
    private String[][] scedule = new String[7][2];
    public Human(String name, String surname, Integer dateOfBirth) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
    }
    public Human(String name, String surname, Integer dateOfBirth, Human father, Human mother) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.father = father;
        this.mother = mother;
    }
    public Human(String name, String surname, Integer dateOfBirth, Human father, Human mother, Integer IQ, Pet pet) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.father = father;
        this.mother = mother;
        this.pet = pet;
        this.IQ = IQ;
    }
    public Human()
    {}
    /**
     * Human{name='Michael', surname='Karleone', year=1977, iq=90,
     * mother=Jane Karleone, father=Vito Karleone, pet=dog{nickname='Rock', age=5, trickLevel=75, habits=[eat, drink, sleep]}}
     * */
    public void gretingPet() { System.out.printf("Hello, %s",this.pet.getNickname()); }
    public void descripePet() {
        String desctipe= new String("");
        String formatted = String.format("I have a %s, he is %d years old, ",this.pet.getSpecies(), this.pet.getAge() );
        /*I have a [species], he is [age] years old, he is [very sly]>50/[almost not sly]<50".*/
        if (pet.getTrickLevel() < 50)
            formatted += "he is almost not sly";
        else
            formatted += "very sly";
        System.out.println(formatted);
    }
    public String getName() { return this.name; }
    @Override
    public String toString() {
        System.out.printf("Human{name='%s', surname='%s', year=%d, iq = %d, mother = %s, " +
                        "father = %s, pet = dog{nickname='%s', age= %d, trickLevel=%d, habits=[eat,drink,sleep]}}", this.name,this.surname,this.dateOfBirth,this.IQ,this.mother.getName(),
                this.father.getName(),this.pet.getNickname(),this.pet.getAge(),this.pet.getTrickLevel());
        return null;
    }
}

public class Familyhw4 {
    public static void main(String[] args) {
        Human mother = new Human("Jane", "Karleone", 1980);
        Human father = new Human("Vito","Karleone",1982);
        Pet dog = new Pet( new String[]{"eat", "drink","sleep"}, "Rock", 5,75);
        Human child = new Human("Michael", "Karleone",1999, father,mother,120,dog);

    }
}
