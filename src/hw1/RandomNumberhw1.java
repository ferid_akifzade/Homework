package hw1;

import java.util.Random;
import java.lang.Math;
import java.util.Scanner;
public class RandomNumberhw1 {

    private static void numberGuessGame()
    {
        System.out.println("Welcome to number guess game.");
        Random rand = new Random(100);
        int secretNumber = Math.abs(rand.nextInt());
        System.out.println("Number is selected lets try to find it. Write any of number in 0-100 range");
        String guessedNumbersArray = "";
        int guessedNumber = 500;
        while (true) {
            Scanner inputNumber = new Scanner(System.in);
            guessedNumber = inputNumber.nextInt();
            if (guessedNumber <= 100 || guessedNumber >= 0) {
                guessedNumbersArray += Integer.toString(guessedNumber) + ", ";
                if (guessedNumber > secretNumber)
                    System.out.println("You are going too high. Go down man");
                else if (guessedNumber < secretNumber)
                    System.out.println("You are going too low. Go up man");
                else {
                    System.out.println("You found number. Congratulation");
                    break;
                }
            } else {
                System.out.println("Please enter number in valid range");
            }
        }

        System.out.printf("Your inputs : %s", guessedNumbersArray);
    }
    private static void wellKnownEventGame() {
        System.out.println("Welcome to Weel knows events game");
        System.out.println("Question 1: When did the World War II begin?(enter year)");
        while (true) {
            Scanner userInput = new Scanner(System.in);
            if (userInput.nextInt() == 1939) {
                System.out.println("You are right! Congratulation!");
                break;
            }
            else
                System.out.println("Sorry. It's wrong answer. Try again");
        }
    }
    public static void main(String[] args) {
        System.out.printf("Select your game : 1) Secret number %n game 2)Well known events %n Type 0 to exit (1,2 or 0)");
        Scanner selectGame = new Scanner(System.in);
        switch (selectGame.nextInt()) {
            case 1:
                numberGuessGame();
                break;
            case 2:
                wellKnownEventGame();
                break;

        }
    }



}
