package hw5;
import java.util.ArrayList;
import java.util.Objects;

class Family{
    private Human mother,father;
    private ArrayList<Human> child;
    private Pet pet;
    private Integer childNumber = 0;

    /* <Getters> */
    public Human getMother() { return mother; }
    public Human getFather() { return father; }
    public ArrayList<Human> getChild() { return child; }
    public Pet getPet() { return pet; }
    /* </Getters> */


    /* <Setters> */
    public void setMother(Human mother) { this.mother = mother; }
    public void setFather(Human father) { this.father = father; }
    public void setChild(ArrayList<Human> child) { this.child = child; }
    public void setPet(Pet pet) { this.pet = pet; }
    /* </Setters> */

    @Override
    public int hashCode() {
        return Objects.hash(mother, father, child, pet, childNumber);
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) return true;
        if (that == null || getClass() != that.getClass()) return false;
        Family family = (Family) that;
        if (childNumber != 0)
            return mother.equals(family.mother) && father.equals(family.father) && pet.equals(family.pet) &&
                    child.equals(family.child);

        return mother.equals(family.mother) && father.equals(family.father) && pet.equals(family.pet);
    }


    void deleteChild(Human child)
    {
        ArrayList<Human> myChild = new ArrayList<Human>();
        myChild.remove(child);
        this.child = myChild;
        childNumber -= 1;
    }

    void addChild(Human child)
    {
        ArrayList<Human> myChild = new ArrayList<Human>();
        myChild.add(child);
        this.child = myChild;
        childNumber += 1;
    }

    Integer count() {
        return 2+childNumber;
    }

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
    }
    public Family(Human mother, Human father, Pet pet) {
        this.mother = mother;
        this.father = father;
        this.pet = pet;
    }




}
