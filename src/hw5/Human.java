package hw5;
import java.util.Arrays;
import java.util.Objects;

class Human{
    private String name, surname;
    private Integer IQ, dateOfBirth;
    private Human mother, father;
    private String[][] scedule = new String[7][2];
    private Family family;
    private void setFamily(Human mother,Human father)
    {
        family.setFather(father);
        family.setMother(mother);
    }
    /* <Setters> */
    public void setName(String name) { this.name = name; }
    public void setSurname(String surname) { this.surname = surname; }
    public void setIQ(Integer IQ) { this.IQ = IQ; }
    public void setDateOfBirth(Integer dateOfBirth) { this.dateOfBirth = dateOfBirth; }
    public void setMother(Human mother) { this.mother = mother; }
    public void setFather(Human father) { this.father = father; }
    public void setScedule(String[][] scedule) { this.scedule = scedule; }
    /* </Setters>*/


    /* <Getters> */
    public Integer getDateOfBirth() { return dateOfBirth; }
    public String getName() { return this.name; }
    public String getSurname(){return this.surname;}
    public Integer getIQ(){return  this.IQ;}
    public Human getMother(){return this.mother;}
    public Human getFather() {return this.father;}
    public String[][] getScedule() { return scedule; }
    /* </Getters> */


    /* <Constructors> */
    public Human(String name, String surname, Integer dateOfBirth, Human father, Human mother) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.father = father;
        this.mother = mother;
        setFamily(mother,father);
    }
    public Human(String name, String surname, Integer dateOfBirth, Human father, Human mother, Integer IQ) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.father = father;
        this.mother = mother;
        this.IQ = IQ;
        setFamily(mother,father);

    }
    public Human(String name, String surname, Integer dateOfBirth, Integer IQ) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.IQ = IQ;
    }
    public Human(String name, String surname, Integer dateOfBirth, Integer IQ, java.lang.String[][] scedule) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.scedule = scedule;
    }
    public Human() {}
    /* </Constructors> */

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", IQ=" + IQ +
                ", year=" + dateOfBirth +
                ", scedule=" + Arrays.toString(scedule) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return name.equals(human.name) &&
                surname.equals(human.surname) &&
                IQ.equals(human.IQ) &&
                dateOfBirth.equals(human.dateOfBirth) &&
                Arrays.equals(scedule, human.scedule);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, surname, IQ, dateOfBirth);
        result = 31 * result + Arrays.hashCode(scedule);
        return result;
    }
}

