package hw5;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {

    @org.junit.jupiter.api.Test
    void testEquals() {

        Pet dog = new Pet( new String[]{"eat", "drink","sleep"}, "Rock", 5,75);
        Family family1 = new Family(new Human("Jane", "Karleone", 1980,100),
                new Human("Vito","Karleone",1982,100),dog);;

        Family family2 = new Family(new Human("Jane", "Karleone", 1980,100),
                new Human("Vito","Karleone",1982,100),dog);;

        boolean expected = family1.equals(family2);
        boolean current = true;
        assertEquals(expected,current);

    }

    @org.junit.jupiter.api.Test
    void count() {
        Pet dog = new Pet( new String[]{"eat", "drink","sleep"}, "Rock", 5,75);

        Family family1 = new Family(new Human("Jane", "Karleone", 1980,100),
                new Human("Vito","Karleone",1982,100),dog);
        family1.addChild(new Human("Michael", "Karleone",1999,100));
        int familyMemberNumberBefore = family1.count();
        family1.addChild(new Human("Michael", "Karleone",1999,100));
        int familyMemberNumberAfter = family1.count();
        assertEquals(familyMemberNumberAfter-1,familyMemberNumberBefore);
    }

    @org.junit.jupiter.api.Test
    void deleteChild() {
        Pet dog = new Pet( new String[]{"eat", "drink","sleep"}, "Rock", 5,75);
        Family family1 = new Family(new Human("Jane", "Karleone", 1980,100),
                new Human("Vito","Karleone",1982,100),dog);;

        Human child = new Human("Michael", "Karleone",1999,100);
        family1.addChild(child);
        family1.deleteChild(child);
        int actual = family1.count()-2;
        int expected = 0;
        assertEquals(expected,actual);

    }

    @org.junit.jupiter.api.Test
    void addChild() {
        Pet dog = new Pet( new String[]{"eat", "drink","sleep"}, "Rock", 5,75);
        Family family1 = new Family(new Human("Jane", "Karleone", 1980,100),
                new Human("Vito","Karleone",1982,100),dog);
        int currentChild = family1.count();

        family1.addChild(new Human("Michael", "Karleone",1999,100));
        int afterChild = family1.count();

        assertEquals(currentChild,afterChild-1);
    }



}