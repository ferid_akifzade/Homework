package hw5;

public class myFamily {
    public static void main(String[] args) {
        Pet dog = new Pet( new String[]{"eat", "drink","sleep"}, "Rock", 5,75);
        Family myFamily = new Family(new Human("Jane", "Karleone", 1980,100),
                new Human("Vito","Karleone",1982,100),dog);
        myFamily.addChild(new Human("Michael", "Karleone",1999,100));
        System.out.println(myFamily.count());
    }
}
