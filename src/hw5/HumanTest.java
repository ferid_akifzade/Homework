package hw5;

import java.lang.String;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {

    @Test
    void testEquals() {
        Human h1 = new Human("Jane", "Karleone", 1980,100);
        Human h2 = new Human("Jane", "Karleone", 1980,100);
        boolean current = h1.equals(h2);
        boolean expected = true;
        assertEquals(current,expected);
    }

    @Test
    void setName() {
        Human human = new Human("Jane", "Karleone", 1980,100);
        human.setName("John");
        String expected = "John";
        String actual = human.getName();
    }

    @Test
    void setSurname() {
        Human human = new Human("Jane", "Karleone", 1980,100);
        human.setSurname("John");
        String expected = "John";
        String actual = human.getSurname();
        assertEquals(expected,actual);
    }

    @Test
    void setIQ() {
        Human human = new Human("Jane", "Karleone", 1980,100);
        human.setIQ(120);
        Integer expected = 120;
        Integer actual = human.getIQ();
        assertEquals(expected,actual);
    }

    @Test
    void setDateOfBirth() {
        Human human = new Human("Jane", "Karleone", 1980,100);
        human.setDateOfBirth(120);
        Integer expected = 120;
        Integer actual = human.getDateOfBirth();
        assertEquals(expected,actual);
    }

    @Test
    void setScedule() {
        Human human = new Human("Jane", "Karleone", 1980,100, new String[][]{{"Monday", "Task"}});
        String[][] expected = {{"Wednesday" , "task"}};
        human.setScedule(expected);
        String[][] actual = human.getScedule();
        assertEquals(expected,actual);
    }

    @Test
    void getDateOfBirth() {
        Human human = new Human("Jane", "Karleone", 1980,100);
        int actual = human.getDateOfBirth();
        int expected = 1980;
        assertEquals(expected,actual);
    }

    @Test
    void getName() {
        Human human = new Human("Jane", "Karleone", 1980,100);
        String actual = human.getName();
        String expected = "Jane";
        assertEquals(expected,actual);
    }

    @Test
    void getSurname() {
        Human human = new Human("Jane", "Karleone", 1980,100);
        String actual = human.getSurname();
        String expected = "Karleone";
        assertEquals(expected,actual);
    }

    @Test
    void getIQ() {
        Human human = new Human("Jane", "Karleone", 1980,100);
        int actual = human.getIQ();
        int expected = 100;
        assertEquals(expected,actual);
    }

    @Test
    void getScedule() {
        String[][] expected = {{"Monday"},{"Task"}};
        Human human = new Human("Jane", "Karleone", 1980,100, expected);
        String[][] actual = human.getScedule();

        assertEquals(expected[0][0], actual[0][0]);
        assertEquals(expected[1][0], actual[1][0]);

    }
}