package hw5;

import java.util.Arrays;
import java.util.Objects;

class Pet {
    private String nickname,species;
    private Integer age, trickLevel;
    private String[] habbits = new String[3];

    /* Constructors */
    Pet(String[] habbits, String nickname, int age, int trickLevel) {
        this.habbits = habbits;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
    }

    /* <Getters> */
    public String getNickname() { return this.nickname; }
    public Integer getAge()  { return this.age; }
    public Integer getTrickLevel()  { return this.trickLevel; }
    public String getSpecies(){return this.species;}
    public String getHabbits()  { return Arrays.toString(this.habbits); }
    /* </Getters> */

    /* <Setters> */
    public void setNickname(String nickname) { this.nickname = nickname; }
    public void setSpecies(String species) { this.species = species; }
    public void setAge(Integer age) { this.age = age; }
    public void setTrickLevel(Integer trickLevel) { this.trickLevel = trickLevel; }
    public void setHabbits(String[] habbits) { this.habbits = habbits; }
    /* </Setters> */

    @Override
    public boolean equals(Object that) {
        if(this == that) return true;
        if(!(that instanceof Pet)) return false;
        Pet myPet = (Pet) that;
        return this.nickname.equals(myPet.getNickname()) && this.species.equals(myPet.getSpecies())
                && this.trickLevel.equals(myPet.getTrickLevel()) && this.age.equals(myPet.getAge()) &&
                this.species.equals(myPet.species);

    }

    @Override
    public int hashCode() {
        int result = Objects.hash(nickname, species, age, trickLevel);
        result = 31 * result + Arrays.hashCode(habbits);
        return result;
    }

    @Override
    public String toString() {
        System.out.printf("dog{nickname='%s', age = %d, trickLevel = %d, habit=[%s]}, where dog = species;",
                this.nickname,this.age,this.trickLevel, Arrays.toString(this.habbits) );
        return null;
    }
    public void eat() { System.out.println("I am eating"); }
    public void respond() { System.out.printf("Hello, owner. I am %s. I miss you!", this.nickname); }
    public void foul() { System.out.println("I need to cover it up"); }


}
