package hw6;

import java.util.*;
import java.util.ArrayList;
import java.util.Arrays;

class Human {
    private String name, surname;
    private Integer IQ, dateOfBirth;
    private Human mother, father;
    private String[][] schedule = {{DaysOfWeek.MONDAY.name()},{DaysOfWeek.TUESDAY.name()},{DaysOfWeek.WEDNESDAY.name()},
            {DaysOfWeek.THURSDAY.name()}, {DaysOfWeek.FRIDAY.name()}, {DaysOfWeek.SATURDAY.name()},{DaysOfWeek.SUNDAY.name()}};


    /* <Setters> */
    public void setName(String name) { this.name = name; }
    public void setSurname(String surname) { this.surname = surname; }
    public void setIQ(Integer IQ) { this.IQ = IQ; }
    public void setDateOfBirth(Integer dateOfBirth) { this.dateOfBirth = dateOfBirth; }
    public void setMother(Human mother) { this.mother = mother; }
    public void setFather(Human father) { this.father = father; }
    public void setschedule(String[][] schedule) { this.schedule = schedule; }
    /* </Setters>*/


    /* <Getters> */
    public Integer getDateOfBirth() { return dateOfBirth; }
    public String getName() { return this.name; }
    public String getSurname(){return this.surname;}
    public Integer getIQ(){return  this.IQ;}
    public Human getMother(){return this.mother;}
    public Human getFather() {return this.father;}
    public String[][] getschedule() { return schedule; }
    /* </Getters> */


    /* <Constructors> */
    public Human(String name, String surname, Integer dateOfBirth, Human father, Human mother) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.father = father;
        this.mother = mother;
    }
    public Human(String name, String surname, Integer dateOfBirth, Human father, Human mother, Integer IQ) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.father = father;
        this.mother = mother;
        this.IQ = IQ;
    }
    public Human(String name, String surname, Integer dateOfBirth, Integer IQ) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        this.IQ = IQ;
    }
    public Human(String name, String surname, Integer dateOfBirth, Integer IQ, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.dateOfBirth = dateOfBirth;
        fillSchedule(schedule);
    }
    public Human() {}
    /* </Constructors> */

    public boolean fillSchedule(String[][] schedule)
    {
        boolean result = false;
        Integer scheduleLenght = schedule.length;
        DaysOfWeek[] days = DaysOfWeek.values();
        for (int i = 0; i < scheduleLenght; i++) {
            result = false;
            for (int j = 0; j < 7; j++) {

               /* if (days[j].name().equals(schedule[i][0].toUpperCase())) {
                    schedule[j][1] += Arrays.toString(schedule[i]);
                }
                */

            }
        }
        return result;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", IQ=" + IQ +
                ", year=" + dateOfBirth +
                ", schedule=" + Arrays.toString(schedule) +
                '}';
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, surname, IQ, dateOfBirth);
        result = 31 * result + Arrays.hashCode(schedule);
        return result;
    }

    @Override
    public boolean equals(Object that) {
        if(this == that) return true;
        if(!(that instanceof Human)) return false;
        Human myHuman = (Human) that;
        return this.name.equals(myHuman.getName()) && this.surname.equals(myHuman.getSurname()) &&
                this.IQ.equals(myHuman.getIQ()) && this.dateOfBirth.equals(myHuman.getDateOfBirth())
                && Arrays.deepEquals(this.schedule, myHuman.getschedule());
    }
    @Override
    protected void finalize() throws Throwable {
        System.out.println("Human deleted");
    }

}
