package hw6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {

    @Test
    void fillScheduleOneDayOneTask() {
        Human testHuman = new Human("Jane", "Karleone", 1980,100);
        String scedule[][] = {{"Monday", "One task"}};
        boolean actual = testHuman.fillSchedule(scedule);
        boolean expected = true;
        assertEquals(expected,actual);
    }
    @Test
    void fillScheduleOneDayTwoTask() {
        Human testHuman = new Human("Jane", "Karleone", 1980,100);
        String scedule[][] = {{"Monday", "One task", "Two task"}};
        boolean actual = testHuman.fillSchedule(scedule);
        boolean expected = true;

    }
    @Test
    void fillScheduleTwoDayOneTask() {
        Human testHuman = new Human("Jane", "Karleone", 1980,100);
        String scedule[][] = {{"Monday", "One task"},{"Wednesday", "One task"}};
        boolean actual = testHuman.fillSchedule(scedule);
        boolean expected = true;

    }
    @Test
    void fillScheduleTwoDayTwoTask() {
        Human testHuman = new Human("Jane", "Karleone", 1980,100);
        String scedule[][] = {{"Monday", "One task", "Two task"}, {"Wednesday", "One task", "Two task"}};
        boolean actual = testHuman.fillSchedule(scedule);
        boolean expected = true;

    }
}