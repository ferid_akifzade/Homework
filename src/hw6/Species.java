package hw6;

public enum Species {
    CAT,
    DOG,
    FISH,
    HORSE,
    CROCODILE,
    TARANTULA
}
